CODE_DIR=CodeDir
BUILD_DIR=build
DATA_DIR=ExtData
TARGET=gcclassic
DCMAKE_Fortran_FLAGS="-O3" # -march=native"
DCMAKE_Fortran_COMPILER=gfortran
DRYRUN_LOG=dryrun.log
RC_FILES=$(wildcard *.rc*)
YAML_FILES=$(wildcard *.yml)

$(info $(RC_FILES))

all : $(TARGET)

$(DRYRUN_LOG): $(TARGET) $(DATA_DIR) $(RC_FILES) $(YAML_FILES)
	./$(TARGET) --dryrun > $@

download : $(DRYRUN_LOG) | $(DATA_DIR)
	./download_data.py $< washu

$(TARGET) : $(CODE_DIR) | $(BUILD_DIR)
	cd $(BUILD_DIR) &&\
		cmake ../$(CODE_DIR) -DRUNDIR=.. -DCMAKE_Fortran_COMPILER=$(DCMAKE_Fortran_COMPILER) -DCMAKE_Fortran_FLAGS=$(DCMAKE_Fortran_FLAGS) &&\
		make -j 16 install

$(CODE_DIR) :
	git clone --depth 1 --branch 14.3.1 --recurse-submodules https://github.com/geoschem/GCClassic.git CodeDir

$(BUILD_DIR) :
	mkdir $(BUILD_DIR)

$(DATA_DIR) : 
	mkdir -p $(DATA_DIR)/HEMCO

.PHONY: clean
clean:
	yes | rm -fr $(CODE_DIR) $(BUILD_DIR) $(TARGET) $(DRYRUN_LOG)

