# This run directory is for GEOS-Chem Classic.

This is a run directory for a CO2 simulation with GEOS-Chem. The Makefile will 

  * Download the GCClassic code
  * Configure and compile the `gcclassic` binary.
  * Download the input data set

A sample `sbatch` script is provided to run the model on Slurm.

## Running

```bash
git clone https://git.ecdf.ed.ac.uk/ddekler/gc_dell_benchmark_co2_nest_europe_default_coarse
cd gc_dell_benchmark_co2_nest_europe_default_coarse
module load gcc/11.2.0 cmake/3.21.3 netcdf/gcc/64/4.8.1
make -j 16 download
sbatch -c 16 sbatch.sh
```

## For instructions on setting up, compiling, and running GEOS-Chem Classic, see:

  - https://geos-chem.readthedocs.org

## For additional information about GEOS-Chem, see:

  - http://wiki.geos-chem.org

  - http://geos-chem.org

## For help with GEOS-Chem Classic, see:

  - https://geos-chem.readthedocs.io/en/latest/reference/SUPPORT.html

  - https://geos-chem.readthedocs.io/en/latest/reference/CONTRIBUTING.html

