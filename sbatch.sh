#!/bin/bash

#SBATCH -o gc_co2_%A.out
#SBATCH -e gc_co2_%A.out

echo "running gcclassic on $SLURM_CPUS_PER_TASK OpenMP threads."

time srun -c $SLURM_CPUS_PER_TASK ./gcclassic
